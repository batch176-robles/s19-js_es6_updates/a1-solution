const getCube = (num) => {
    console.log(`The cube of ${num} is ${num ** 3}`)
}
getCube(2)

const address = ["15 San Pedro St.", "Kapitolyo", "Pasig City", "1603"]

const [street, brgy, city, zip] = address;

console.log(`I live at ${street} ${brgy} ${city} ${zip} `)

const animal = {
    pet: "Lolong",
    type: "saltwater crocodile",
    weight: "1075 kgs",
    height: "20 ft 3 in"
}

const { pet, type, weight, height } = animal

console.log(`${pet} was a ${type}. He weighed at ${weight} with a measurement of ${height}`)

const num = [1, 2, 3, 4, 5]

num.forEach((num1) => {
    console.log(num1)
})

class Dog {
    constructor(name, age, breed) {
        this.name = name,
            this.age = age,
            this.breed = breed
    }
}

const myDog = new Dog('Frankie', 5, "Miniature Dachshund")
console.log(myDog)